
﻿using UnityEngine;

using UnityEngine;

using System.Collections;
using System.Collections.Generic;

public class GameWorld : MonoBehaviour
{

    Dictionary<int, int> dictMoveable = new Dictionary<int, int>();

    public LayerMask notWalkable;

    LayerMask moveable;

    public Vector2 WorldSize;

    int minPen = int.MaxValue;

    int maxPen = int.MinValue;

    public float radNode;

    int mWorldX, mWorldY;


    public TerrainType[] regionsMoveable;

    public int penaltyObs = 50;

    public bool displayGridGizmos;

    WorldNodes[,] mWorld;

    float diamNode;

    
    //To initializing X and Y of the World
    void Awake()
    {
        diamNode = radNode * 2;

        mWorldX = Mathf.RoundToInt(WorldSize.x / diamNode);

        mWorldY = Mathf.RoundToInt(WorldSize.y / diamNode);

        foreach (TerrainType region in regionsMoveable)
        {

            moveable.value |= region.terrainMask.value;

            dictMoveable.Add((int)Mathf.Log(region.terrainMask.value,2) , region.terrainPenalty);
        }

        CreateWorld();
    }


    //To get all the neighbourn nodes of current node
    public List<WorldNodes> GetAdjNodes(WorldNodes node)
    {
        List<WorldNodes> adjNodes = new List<WorldNodes>();

        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0)
                    continue;

                int cX = node.gameWorldX + x;

                int cY = node.gameWorldY + y;

                if (cX >= 0 && cX < mWorldX && cY >= 0 && cY < mWorldY)
                {
                    adjNodes.Add(mWorld[cX, cY]);
                }
            }
        }

        return adjNodes;
    }

    //Creating the World by looking at walkable and non walkable areas
    void CreateWorld()
    {
        mWorld = new WorldNodes[mWorldX, mWorldY];

        Vector3 worldBottomLeft = transform.position - Vector3.right * WorldSize.x / 2 - Vector3.forward * WorldSize.y / 2;

        for (int x = 0; x < mWorldX; x++)
        {
            for (int y = 0; y < mWorldY; y++)
            {

                Vector3 worldPoint = worldBottomLeft + Vector3.right * (x * diamNode + radNode) + Vector3.forward * (y * diamNode + radNode);

                bool mAble = !(Physics.CheckSphere(worldPoint, radNode, notWalkable));

                int penMov = 0;

                //Using Ray Cast to observe if it is a moveable region or not and add penalties accordingly.
                Ray r = new Ray(worldPoint + Vector3.up * 50, Vector3.down);

                RaycastHit h;

                if(Physics.Raycast(r, out h, 100, moveable))
                {
                    dictMoveable.TryGetValue(h.collider.gameObject.layer, out penMov);
                }


                if(!mAble)
                {
                    penMov += penaltyObs;
                }




                mWorld[x, y] = new WorldNodes(mAble, worldPoint, x, y, penMov);
            }
        }

        BoxBlurMap(1);
    }



    public int MaxSize
    {
        get
        {
            return mWorldX * mWorldY;
        }
    }


  //Creating a position into a world position
    public WorldNodes PostoWorldPos(Vector3 worldPosition)
    {
        float pX = (worldPosition.x + WorldSize.x / 2) / WorldSize.x;

        float pY = (worldPosition.z + WorldSize.y / 2) / WorldSize.y;

        pX = Mathf.Clamp01(pX);

        pY = Mathf.Clamp01(pY);

        int x = Mathf.RoundToInt((mWorldX - 1) * pX);

        int y = Mathf.RoundToInt((mWorldY - 1) * pY);

        return mWorld[x, y];
    }


    //For visualization
    void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, new Vector3(WorldSize.x, 1, WorldSize.y));

        if (mWorld != null && displayGridGizmos)
        {
            foreach (WorldNodes n in mWorld)
            {

                Gizmos.color = Color.Lerp(Color.white, Color.black, Mathf.InverseLerp(minPen, maxPen, n.moveAway));

                Gizmos.color = (n.allowed) ? Gizmos.color : Color.red;

                Gizmos.DrawCube(n.locationinWorld, Vector3.one * (diamNode));
            }
        }
    }

    //Using Box Blur Algorithm to help object move along the road instead of corners
    void BoxBlurMap(int blurSize)
    {
        int kSize = blurSize * 2 + 1;

        int kRange = (kSize - 1) / 2;

        int[,] horizontalPen = new int[mWorldX, mWorldY];

        int[,] verticalPen = new int[mWorldX, mWorldY];

        for (int y = 0; y < mWorldY; y++)
        {
            for (int x = -kRange; x <= kRange; x++)
            {

                int sX = Mathf.Clamp(x, 0, kRange);

                horizontalPen[0, y] += mWorld[sX, y].moveAway;
            }

            for (int x = 1; x < mWorldX; x++)
            {

                int rInd = Mathf.Clamp(x - kRange - 1, 0, mWorldX);

                int aInd = Mathf.Clamp(x + kRange, 0, mWorldX - 1);

                horizontalPen[x, y] = horizontalPen[x - 1, y] - mWorld[rInd, y].moveAway + mWorld[aInd, y].moveAway;

            }
        }

        for (int x = 0; x < mWorldX; x++)
        {
            for (int y = -kRange; y <= kRange; y++)
            {

                int sY = Mathf.Clamp(y, 0, kRange);

                verticalPen[x, 0] += horizontalPen[x, sY];

            }

            int penBlurr = Mathf.RoundToInt((float)verticalPen[x, 0] / (kSize * kSize));

            mWorld[x, 0].moveAway = penBlurr;

            for (int y = 1; y < mWorldY; y++)
            {

                int rInd = Mathf.Clamp(y - kRange - 1, 0, mWorldY);

                int aInd = Mathf.Clamp(y + kRange, 0, mWorldY - 1);

                verticalPen[x, y] = verticalPen[x, y - 1] - horizontalPen[x, rInd] + horizontalPen[x, aInd];

                penBlurr = Mathf.RoundToInt((float)verticalPen[x, y] / (kSize * kSize));

                mWorld[x, y].moveAway = penBlurr;

                if (penBlurr > maxPen)
                {
                    maxPen = penBlurr;
                }

                if (penBlurr < minPen)
                {
                    minPen = penBlurr;
                }
            }
        }

    }


    //To differentiate between different terrains and add penalties accordingly for moving away
    [System.Serializable]
    public class TerrainType
    {

        public LayerMask terrainMask;

        public int terrainPenalty;

    }
}