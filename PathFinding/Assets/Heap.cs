using UnityEngine;
using System.Collections;
using System;



//Data Structure used to arrange world points
public class Heap<T> where T : IHeapItem<T>
{

    T[] worldPoints;

    int totalworldPoints;


    //Constructor
    public Heap(int maximumSize)
    {
        worldPoints = new T[maximumSize];
    }

    public int Count
    {
        get
        {
            return totalworldPoints;
        }
    }

    //Used to remove elements
    void SortDown(T point)
    {
        while (true)
        {
            int childIndexLeft = point.HeapIndex * 2 + 1;

            int childIndexRight = point.HeapIndex * 2 + 2;

            int swapIndex = 0;

            if (childIndexLeft < totalworldPoints)
            {
                swapIndex = childIndexLeft;

                if (childIndexRight < totalworldPoints)
                {
                    if (worldPoints[childIndexLeft].CompareTo(worldPoints[childIndexRight]) < 0)
                    {
                        swapIndex = childIndexRight;
                    }
                }

                if (point.CompareTo(worldPoints[swapIndex]) < 0)
                {
                    Swap(point, worldPoints[swapIndex]);
                }

                else
                {
                    return;
                }

            }

            else
            {
                return;
            }

        }
    }

    //Adding new items
    public void Add(T point)
    {
        point.HeapIndex = totalworldPoints;

        worldPoints[totalworldPoints] = point;

        SortUp(point);

        totalworldPoints++;
    }

    //Swapping two indexes
    void Swap(T itemA, T itemB)
    {
        worldPoints[itemA.HeapIndex] = itemB;

        worldPoints[itemB.HeapIndex] = itemA;

        int itemAIndex = itemA.HeapIndex;

        itemA.HeapIndex = itemB.HeapIndex;

        itemB.HeapIndex = itemAIndex;
    }


    public bool Contains(T point)
    {
        return Equals(worldPoints[point.HeapIndex], point);
    }


    //Remove first element
    public T RemoveFirst()
    {
        T firstPoint = worldPoints[0];

        totalworldPoints--;

        worldPoints[0] = worldPoints[totalworldPoints];

        worldPoints[0].HeapIndex = 0;

        SortDown(worldPoints[0]);

        return firstPoint;
    }

    public void UpdatePoints(T point)
    {
        SortUp(point);
    }


    //Sorting the heap
    void SortUp(T point)
    {
        int parentIndex = (point.HeapIndex - 1) / 2;

        while (true)
        {
            T parentItem = worldPoints[parentIndex];

            if (point.CompareTo(parentItem) > 0)
            {
                Swap(point, parentItem);
            }

            else
            {
                break;
            }

            parentIndex = (point.HeapIndex - 1) / 2;
        }
    }

}

public interface IHeapItem<T> : IComparable<T>
{
    int HeapIndex
    {
        get;

        set;
    }
}