using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System;


//Class implementing the A star algorithm
public class AStarPathfinding : MonoBehaviour
{

    GameWorld myWorld;

    //To get whole map
    void Awake()
    {
        myWorld = GetComponent<GameWorld>();
    }

    //Calclating the main path using all costs and given nodes.
    public void MainPath(SeekPath request, Action<FinalizedPath> callback)
    {

        //Stopwatch sw = new Stopwatch();

       // sw.Start();

        Vector3[] pointsLocation = new Vector3[0];

        bool targetFound = false;

        WorldNodes startPoint = myWorld.PostoWorldPos(request.pathStart);

        WorldNodes targetPoint = myWorld.PostoWorldPos(request.pathEnd);

        startPoint.parent = startPoint;


        if (startPoint.allowed && targetPoint.allowed)
        {
            Heap<WorldNodes> toTraverse = new Heap<WorldNodes>(myWorld.MaxSize);

            HashSet<WorldNodes> alreadyTraversed = new HashSet<WorldNodes>();

            toTraverse.Add(startPoint);

            while (toTraverse.Count > 0)
            {

                WorldNodes currentPoint = toTraverse.RemoveFirst();

                alreadyTraversed.Add(currentPoint);

                if (currentPoint == targetPoint)
                {
                    //sw.Stop();
                    //print ("Path found: " + sw.ElapsedMilliseconds + " ms");

                    targetFound = true;

                    break;
                }

                foreach (WorldNodes adjacentPoints in myWorld.GetAdjNodes(currentPoint))
                {

                    if (!adjacentPoints.allowed || alreadyTraversed.Contains(adjacentPoints))

                    {
                        continue;
                    }

                    int newMovementCostToadjacentPoints = currentPoint.gCost + DistbwNodes(currentPoint, adjacentPoints) + adjacentPoints.moveAway;

                    if (newMovementCostToadjacentPoints < adjacentPoints.gCost || !toTraverse.Contains(adjacentPoints))
                    {
                        adjacentPoints.gCost = newMovementCostToadjacentPoints;

                        adjacentPoints.hCost = DistbwNodes(adjacentPoints, targetPoint);

                        adjacentPoints.parent = currentPoint;

                        if (!toTraverse.Contains(adjacentPoints))
                        {
                            toTraverse.Add(adjacentPoints);
                        }

                        else
                        {
                            toTraverse.UpdatePoints(adjacentPoints);
                        }
                    }
                }
            }
        }

        if (targetFound)
        {

            pointsLocation = BacktrackPath(startPoint, targetPoint);

            targetFound = pointsLocation.Length > 0;
        }

        callback(new FinalizedPath(pointsLocation, targetFound, request.callback));

    }

    //After target has been found, retrace to get the final path
    Vector3[] BacktrackPath(WorldNodes startPoint, WorldNodes endNode)
    {
        List<WorldNodes> path = new List<WorldNodes>();

        WorldNodes currentPoint = endNode;

        while (currentPoint != startPoint)
        {
            path.Add(currentPoint);

            currentPoint = currentPoint.parent;
        }

        Vector3[] pointsLocation = CreatePointsLoc(path);

        Array.Reverse(pointsLocation);

        return pointsLocation;

    }

    //To create waypoints only when there is a change in direction
    Vector3[] CreatePointsLoc(List<WorldNodes> path)
    {
        List<Vector3> pointsLocation = new List<Vector3>();

        Vector2 directionOld = Vector2.zero;

        for (int i = 1; i < path.Count; i++)
        {
            Vector2 directionNew = new Vector2(path[i - 1].gameWorldX - path[i].gameWorldX, path[i - 1].gameWorldY - path[i].gameWorldY);

            if (directionNew != directionOld)
            {
                pointsLocation.Add(path[i].locationinWorld);
            }

            directionOld = directionNew;
        }

        return pointsLocation.ToArray();
    }

    //Using formula 14y + 10(x-y)to calculate distance between two world points/nodes
    int DistbwNodes(WorldNodes pointA, WorldNodes pointB)
    {
        int xDistance = Mathf.Abs(pointA.gameWorldX - pointB.gameWorldX);

        int yDistance = Mathf.Abs(pointA.gameWorldY - pointB.gameWorldY);

        if (xDistance > yDistance)
        {
            return 14 * yDistance + 10 * (xDistance - yDistance);
        }

        return 14 * xDistance + 10 * (yDistance - xDistance);
    }


}