using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Class that helps in smoothing the path by changing direction before reaching the waypoint
public class FinalPath
{
    public readonly int endLine;

    public readonly int lessSpeed;

    public readonly Vector3[] locinWorld;

    public readonly Line[] limitforTurn;

    
    //Giving values to all the defined variables 
    public FinalPath(Vector3[] locations, Vector3 startPos, float distoTurn, float distoStop)
    {
        locinWorld = locations;

        limitforTurn = new Line[locinWorld.Length];

        endLine = limitforTurn.Length - 1;

        Vector2 preLoc = ConvertV3toV2(startPos);

        for (int i = 0; i < locinWorld.Length; i++)
        {
            Vector2 currLoc = ConvertV3toV2(locinWorld[i]);

            Vector2 dirCurrLoc = (currLoc - preLoc).normalized;

            Vector2 locTurnLimit = (i == endLine) ? currLoc : currLoc - dirCurrLoc * distoTurn;

            limitforTurn[i] = new Line(locTurnLimit, preLoc - dirCurrLoc * distoTurn);

            preLoc = locTurnLimit;
        }


        float disFinalPoint = 0;

        for (int i = locinWorld.Length - 1; i > 0; i--)
        {

            disFinalPoint += Vector3.Distance(locinWorld[i], locinWorld[i - 1]);

            if (disFinalPoint > distoStop)
            {

                lessSpeed = i;

                break;
            }
        }
    }

    //To Visualize the path with Path smoothing lines
    public void DrawWithGizmos()
    {

        Gizmos.color = Color.black;

        foreach (Vector3 p in locinWorld)
        {
            Gizmos.DrawCube(p + Vector3.up, Vector3.one);
        }


        Gizmos.color = Color.white;

        foreach (Line l in limitforTurn)
        {
            l.DrawWithGizmos(10);
        }

    }

    Vector2 ConvertV3toV2(Vector3 v3)
    {
        return new Vector2(v3.x, v3.z);
    }

    

}