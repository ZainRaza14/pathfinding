using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Threading;

public class PathManager : MonoBehaviour
{

    Queue<FinalizedPath> finishedPaths = new Queue<FinalizedPath>();

    static PathManager instance;

    AStarPathfinding findthePath;

    void Awake()
    {
        instance = this;

        findthePath = GetComponent<AStarPathfinding>();
    }

   
    void Update()
    {
        if (finishedPaths.Count > 0)
        {
            int queueCount = finishedPaths.Count;

            lock (finishedPaths)
            {
                for (int i = 0; i < queueCount; i++)
                {

                    FinalizedPath finalPath = finishedPaths.Dequeue();

                    finalPath.callback(finalPath.path, finalPath.posResult);
                }
            }
        }
    }

    //Using threads to request paths
    public static void RequestPath(SeekPath request)
    {
        ThreadStart threadStart = delegate 
        {
            instance.findthePath.MainPath(request, instance.PathProcessed);
        };

        threadStart.Invoke();

    }

    //Locking the queue so that only one thread can access it a time
    public void PathProcessed(FinalizedPath finalPath)
    {
        lock (finishedPaths)
        {
            finishedPaths.Enqueue(finalPath);
        }
    }



}

//Structure for seekers to request a path
public struct SeekPath
{
    public Vector3 pathStart;

    public Vector3 pathEnd;

    public Action<Vector3[], bool> callback;

    public SeekPath(Vector3 _start, Vector3 _end, Action<Vector3[], bool> _callback)
    {
        pathStart = _start;

        pathEnd = _end;

        callback = _callback;
    }


}


//Structure used by A star to send the finalized path with waypoints
public struct FinalizedPath
{

    public bool posResult;

    public Action<Vector3[], bool> callback;

    public Vector3[] path;


    public FinalizedPath(Vector3[] path, bool posResult, Action<Vector3[], bool> callback)
    {
        this.path = path;

        this.posResult = posResult;

        this.callback = callback;
    }

}

