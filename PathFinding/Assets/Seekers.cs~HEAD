﻿using UnityEngine;
using System.Collections;

//Class used by all the seekers to call for the algorithm to move towards target
public class Seekers : MonoBehaviour
{

    const float letpathToChange = .2f;

    const float pathUpdateMoveThreshold = .5f;

    public Transform target;

    public float speed = 20;

    public float turnSpeed = 3;

    public float turnDst = 5;

    public float stoppingDst = 10;

    FinalPath path;

    void Start()
    {
        StartCoroutine(pathToChange());
    }


    //After path has been finalized start moving along tha path
    public void truePath(Vector3[] locsinWorld, bool pathToTraverse)
    {
        if (pathToTraverse)
        {
            path = new FinalPath(locsinWorld, transform.position, turnDst, stoppingDst);

            StopCoroutine("MoveAlongPath");

            StartCoroutine("MoveAlongPath");
        }
    }


    //Request a path from  path manager and wait for few time to call next path
    IEnumerator pathToChange()
    {

        if (Time.timeSinceLevelLoad < .3f)
        {
            yield return new WaitForSeconds(.3f);
        }

        PathManager.RequestPath(new SeekPath(transform.position, target.position, truePath));


        float sqrMoveThreshold = pathUpdateMoveThreshold * pathUpdateMoveThreshold;

        Vector3 targetPosOld = target.position;

        //Instead of calling path every frame only call it if target move from its old position
        while (true)
        {
            yield return new WaitForSeconds(letpathToChange);

            //print(((target.position - targetPosOld).sqrMagnitude) + "    " + sqrMoveThreshold);

            if ((target.position - targetPosOld).sqrMagnitude > sqrMoveThreshold)
            {

                PathManager.RequestPath(new SeekPath(transform.position, target.position, truePath));

                targetPosOld = target.position;
            }
        }
    }

    //Function to allow seekers to move along curved roads with path smoothing

    IEnumerator MoveAlongPath()
    {

        bool movingPath = true;

        int numbPath = 0;

        transform.LookAt(path.locinWorld[0]);

        float finalSpeed = 1;

        while (movingPath)
        {
            Vector2 location2D = new Vector2(transform.position.x, transform.position.z);

            while (path.limitforTurn[numbPath].HasCrossedLine(location2D))
            {
                if (numbPath == path.endLine)
                {
                    movingPath = false;
                    break;
                }

                else
                {
                    numbPath++;
                }
            }

            if (movingPath)
            {

                if (numbPath >= path.lessSpeed && stoppingDst > 0)
                {
                    finalSpeed = Mathf.Clamp01(path.limitforTurn[path.endLine].PointDist(location2D) / stoppingDst);

                    if (finalSpeed < 0.01f)
                    {
                        movingPath = false;
                    }
                }

                Quaternion rotTarget = Quaternion.LookRotation(path.locinWorld[numbPath] - transform.position);

                transform.rotation = Quaternion.Lerp(transform.rotation, rotTarget, Time.deltaTime * turnSpeed);

                transform.Translate(Vector3.forward * Time.deltaTime * speed * finalSpeed, Space.Self);
            }

            yield return null;

        }
    }

    //Function to show the path points 
    public void OnDrawGizmos()
    {
        if (path != null)
        {
            path.DrawWithGizmos();
        }
    }
}