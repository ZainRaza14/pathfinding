using UnityEngine;
using System.Collections;

//Class used to store location points which is later used to move the seeker
public class WorldNodes : IHeapItem<WorldNodes>
{

    public bool allowed;

    public Vector3 locationinWorld;

    public int gCost , hCost;

    public WorldNodes parent;

    public int gameWorldX, gameWorldY;

    public int moveAway;

    int heapIndex;


    //Class constructor
    public WorldNodes(bool _allowed, Vector3 _locationinWorld, int _gameWorldX, int _gameWorldY, int _moveAway)
    {
        allowed = _allowed;

        locationinWorld = _locationinWorld;

        gameWorldX = _gameWorldX;

        gameWorldY = _gameWorldY;

        moveAway = _moveAway;
    }

    //Calculating the fCost used in A star algorithm
    public int fCost
    {
        get
        {
            return gCost + hCost;
        }
    }


    //Function used in Heap to sort the Heap.
    public int CompareTo(WorldNodes nodetoMatch)
    {
        int match = fCost.CompareTo(nodetoMatch.fCost);

        if (match == 0)
        {
            match = hCost.CompareTo(nodetoMatch.hCost);
        }

        return -match;
    }

    //Getter and Setter for the HeapIndex
    public int HeapIndex
    {
        get
        {
            return heapIndex;
        }
        set
        {
            heapIndex = value;
        }
    }

    
}