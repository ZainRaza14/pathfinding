using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//A class used to help smooth the path of the object
public struct Line
{

    const float yGrad = 1e5f;


    float grad;

    float interceptY;

    Vector2 line1Point , line2Point;

    float gradPerp;

    bool mainSide;

    //Initializing variables
    public Line(Vector2 linePoint, Vector2 perpPoint)
    {
        float distX = linePoint.x - perpPoint.x;

        float distY = linePoint.y - perpPoint.y;

        if (distX == 0)
        {
            gradPerp = yGrad;
        }

        else
        {
            gradPerp = distY / distX;
        }

        if (gradPerp == 0)
        {
            grad = yGrad;
        }

        else
        {
            grad = -1 / gradPerp;
        }

        interceptY = linePoint.y - grad * linePoint.x;

        line1Point = linePoint;

        line2Point = linePoint + new Vector2(1, grad);

        mainSide = false;

        mainSide = GetSide(perpPoint);
    }

    //To help slow down speed when near target
    public float PointDist(Vector2 point)
    {
        float interceptYPerp = point.y - gradPerp * point.x;

        float xIntersect = (interceptYPerp - interceptY) / (grad - gradPerp);

        float yIntersect = grad * xIntersect + interceptY;

        return Vector2.Distance(point, new Vector2(xIntersect, yIntersect));
    }

    //To check if the point is whether on one side of line or another
    bool GetSide(Vector2 point)
    {
        return (point.x - line1Point.x) * (line2Point.y - line1Point.y) > (point.y - line1Point.y) * (line2Point.x - line1Point.x);
    }



    //Used by FinalPath to show the path
    public void DrawWithGizmos(float length)
    {

        Vector3 lineDir = new Vector3(1, 0, grad).normalized;

        Vector3 lineCentre = new Vector3(line1Point.x, 0, line1Point.y) + Vector3.up;

        Gizmos.DrawLine(lineCentre - lineDir * length / 2f, lineCentre + lineDir * length / 2f);
    }

    //To know after object has crossed the line
    public bool HasCrossedLine(Vector2 point)
    {
        return GetSide(point) != mainSide;
    }


}